﻿#set variables
$204InPath = "\\swift.com\shared_data\LogisticsB2B\Production\Altova\Inbound\CarrierPoint\204\"
$214InPath = "\\swift.com\shared_data\LogisticsB2B\Production\Altova\Inbound\CarrierPoint\214\"
$CarrierPointInPath = "\\swift.com\shared_data\LogisticsB2B\Production\Altova\Inbound\CarrierPoint\"
$EDIFile = gci $CarrierPointInPath
$LogPath = "\\stcphx-altv3301\d$\scripts\Logs"
$Log = "Parse_Ponderay_EDI_Doctype_Prod.log"
$error.clear()
$MailRecipient1 = "Ponderay Operations <Ponderay_OPS@swifttrans.com>"
$MailRecipient2 = "Kristen Hampton <Kristen_Hampton@swifttrans.com>"
$MailRecipient3 = "Chad <chad_moscarella@swifttrans.com>"
$MailRecipient4 = "Caryn Murray <caryn_murray@swifttrans.com>"
$MailSender = "PROD Middleware Scripts <PROD_PowerShell@SwiftLogistics.com>"
$MailBody = "A load tender indicating a canceled shipment has been received via EDI and is being processed by into the Integration Layer.`r`nThe EDI file that triggered this alert is attached."
$MailServer = "mailrelay.swifttrans.com"

#perform the following on each file (not folder) in $CarrierPointInPath$
foreach ($file in (gci $CarrierPointInPath | where {!$_.PsIsContainer}))
{    

#check document type
$204 = Select-String ST*204* $file -SimpleMatch
$214 = Select-String ST*214* $file -SimpleMatch
$997 = Select-String ST*997* $file -SimpleMatch


#Parse EDI Segments
$B2 = Select-String "B2\*\*" $file
$B2A = Select-String "B2A\*" $file
$L11BN = Select-String "\*BN" $file

$MailSubject1 = "Ponderay shipment #SL$Shipment, $PONDOrderNumber CANCELLED."
$MailBody1 = "This notification is to inform you that the shipment #SL$Shipment, $PONDOrderNumber has been CANCELLED. Please take appropriate measures to update systems."


cd $CarrierPointInPath
    
#copy 997, 204 and 214 files to appropriate directories with new name, then reset $Stamp to prevent duplicate file names.


if ($204) 
    {
        #Tokenize EDI Segments
        $CarrierSCAC = $B2.ToString().split('*')
        $StatusCode = $B2A.ToString().split('*')
        $L11BN = $L11BN.ToString().split('*') 
        $PONDOrderNumber = $L11BN[1]
        $Shipment = $CarrierSCAC[4]
        $MailSubject1 = "Ponderay shipment #SL$Shipment, $PONDOrderNumber CANCELLED."
        $MailBody1 = "This notification is to inform you that the shipment #SL$Shipment, $PONDOrderNumber has been CANCELLED. Please take appropriate measures to update systems."
        
            if ($CarrierSCAC[2] -notlike "SWFG") 
    
                {

                if ($StatusCode[1] -like '01') 
                    {
                        Send-mailmessage -To $MailRecipient1 -From $MailSender -subject $MailSubject1 -body $MailBody1 -SmtpServer $MailServer
                        #Send-mailmessage -To $MailRecipient2 -From $MailSender -subject $MailSubject1 -body $MailBody1 -SmtpServer $MailServer
                        Send-mailmessage -To $MailRecipient3 -From $MailSender -subject $MailSubject1 -body $MailBody1 -SmtpServer $MailServer
                        #Send-mailmessage -To $MailRecipient4 -From $MailSender -subject $MailSubject1 -body $MailBody1 -SmtpServer $MailServer
                        $StatusCode.Clear()

                    }
                }

        $Stamp = get-date -format MMddyyyy_hhmmss_ffffff
        $newname = "CP_204_$stamp.204"
        Copy-Item -path $File -Destination \\swift.com\shared_data\LogisticsB2B\Production\Altova\Inbound\CarrierPoint\PND_204\$newname
        if ($error)
            { Write-Output "$Stamp [ERROR] - $error `r" | Out-File -filepath "$LogPath\$Log"  -NoClobber -append}
        else
            { Write-Output "$Stamp [INFO] $File moved and renamed to $CarrierPointInPath$newname `r" | Out-File -filepath "$LogPath\$Log" -NoClobber -append }
        $Error.Clear()
        
    }

if ($214) 
    { 
        #Tokenize EDI Segments
        $CarrierSCAC = $B2.ToString().split('*')
        $StatusCode = $B2A.ToString().split('*')
        $L11BN = $L11BN.ToString().split('*')
        $PONDOrderNumber = $L11BN[1]
        $Shipment = $CarrierSCAC[4]

        $Stamp = get-date -format MMddyyyy_hhmmss_ffffff
        $newname = "CP_214_$stamp.214"
        Copy-Item -path $File -Destination \\swift.com\shared_data\LogisticsB2B\Production\Altova\Inbound\CarrierPoint\PND_214\$newname
        
        if ($error) 
            { Write-Output "$Stamp [ERROR] - $error `r" | Out-File -filepath "$LogPath\$Log"  -NoClobber -append}
        else
            { Write-Output "$Stamp [INFO] $File moved and renamed to $CarrierPointInPath$newname `r" | Out-File -filepath "$LogPath\$Log" -NoClobber -append}
        $Error.Clear()
    }
if ($997) 
    { 
        $Stamp = get-date -format MMddyyyy_hhmmss_ffffff
        $newname = "CP_997_$stamp.997"
        Copy-Item -path $File -Destination \\swift.com\shared_data\LogisticsB2B\Production\Altova\Inbound\CarrierPoint\PND_997\$newname
        
        if ($error) 
            { Write-Output "$Stamp [ERROR] - $error `r" | Out-File -filepath "$LogPath\$Log"  -NoClobber -append}
        else
            { Write-Output "$Stamp [INFO] $File moved and renamed to $CarrierPointInPath$newname `r" | Out-File -filepath "$LogPath\$Log" -NoClobber -append}
        $Error.Clear()
    }

$newname = "Renamed_$stamp.x12"
Move-Item -path $File -Destination \\swift.com\shared_data\LogisticsB2B\Production\Altova\Inbound\CarrierPoint\Renamed\$newname

#housekeeping

clear-item variable:204
clear-item variable:214
clear-item variable:997

}
