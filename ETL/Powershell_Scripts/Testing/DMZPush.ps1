# Define variables.
# File paths:
# Transport Server
$uncServer = "\\stcphx-cleo3602"

# Outboxes on the Transport Server, by Trading Partner.
$uncMLOutbox = "$uncServer\VLTrader\McLeod\Outbox"
$uncMGOutbox = "$uncServer\VLTrader\MercuryGate\Outbox"
$uncSPSOutbox = "$uncServer\VLTrader\SPSCommerce\Outbox"
$uncCPOutbox = "$uncServer\VLTrader\CarrierPoint\Outbox"

# Outbound folders on the Middleware Server, by Trading Partner.
$uncMLFiles = "D:\B2B\Outbound\McLeod\"
$uncMGFiles = "D:\B2B\Outbound\MercuryGate\"
$uncSPSFiles = "D:\B2B\Outbound\SPSCommerce\"
$uncWDFiles = "D:\B2B\Outbound\CarrierPoint\"

# Files in Outbound folders on the Middleware Server, by Trading Partner.
$MLOutfiles = gci $uncMLFiles\*.* -recurse
$MGOutfiles = gci $uncMGFiles\*.* -recurse
$SPSOutfiles = gci $uncSPSFiles\*.* -recurse
$CPOutfiles = gci $uncWDFiles\*.* -recurse 

#Other variables:
$username = "stcphx-cleo3602\Files"
$password = "P@swrd1"
$datetime = Get-Date 
 
# Set credentials and path for unc share defined above in $uncServer, $uncMGOutbox, $username, and $password.

# I should probably move these variables out to a .cfg file, and consider encrypting the password. 
net use $uncServer $password /USER:$username

# Each of these snipits moves files to the AS2/FTP server with specified name patterns to specified folders on the middleware server, then logs results.

##############################################################################################################################################
#                                                        MOVE SPS FILES TO Transport Server                                                  #
##############################################################################################################################################

foreach ($file in $SPSOutfiles){ 

if ($file.name -match "WD_940*"){
if (!(test-path ("$uncSPSOutbox" + $matches[1]))){new-item ("$uncSPSOutbox" + $matches[1]) -type "directory"}
$newname = $file.name.split(".")[0] + "." + $file.name.split(".")[1]
$error.clear()
move-item $file.fullname ("$uncSPSOutbox" + "\" + $newname) -Force
    if (!$error[0]) { add-content $LogFile "$datetime Copied $($file.fullname) to $($uncSPSOutbox + "\" + $newname) + `r`n  "}
    if ($error[0]) { add-content $LogFile "$datetime --- [ERROR - $file.fullname] --- `r`n $Error`r`n "} 
}

if ($file.name -match "WD_210*"){
if (!(test-path ("$uncSPSOutbox" + $matches[1]))){new-item ("$uncSPSOutbox" + $matches[1]) -type "directory"}
$newname = $file.name.split(".")[0] + "." + $file.name.split(".")[1]
$Error.clear()
move-item $file.fullname ("$uncSPSOutbox" + "\" + $newname) -Force
    if (!$error[0]) { add-content $LogFile "$datetime Copied $($file.fullname) to $($uncSPSOutbox + "\" + $newname) + `r`n  "}
    if ($error[0]) { add-content $LogFile "$datetime --- [ERROR - $file.fullname] --- `r`n $Error`r`n "} 

}

if ($file.name -match "WD_204*"){
if (!(test-path ("$uncSPSOutbox" + $matches[1]))){new-item ("$uncSPSOutbox" + $matches[1]) -type "directory"}
$newname = $file.name.split(".")[0] + "." + $file.name.split(".")[1]
$Error.Clear()
move-item $file.fullname ("$uncSPSOutbox" + "\" + $newname) -Force
    if (!$error[0]) { add-content $LogFile "$datetime Copied $($file.fullname) to $($uncSPSOutbox + "\" + $newname) + `r`n  "}
    if ($error[0]) { add-content $LogFile "$datetime --- [ERROR - $file.fullname] --- `r`n $Error`r`n "} 

}
}

##############################################################################################################################################
#                                                        MOVE CarrierPoint FILES TO Transport Server                                         #
##############################################################################################################################################


foreach ($file in $CPOutfiles){ 

if ($file.name -match "PND_204*"){
if (!(test-path ("$uncCPOutbox" + $matches[1]))){new-item ("$uncCPOutbox" + $matches[1]) -type "directory"}
$error.clear()
move-item $file.fullname ("$uncCPOutbox" + "\" + $newname) -Force
    if (!$error[0]) { add-content $LogFile "$datetime Copied $($file.fullname) to $($uncCPOutbox + "\" + $newname) + `r`n  "}
    if ($error[0]) { add-content $LogFile "$datetime --- [ERROR - $file.fullname] --- `r`n $Error`r`n "} 

}

if ($file.name -match "PND_214"){
if (!(test-path ("$uncCPOutbox" + $matches[1]))){new-item ("$uncCPOutbox" + $matches[1]) -type "directory"}
$newname = $file.name.split(".")[0] + "." + $file.name.split(".")[1]
$error.clear
move-item $file.fullname ("$uncCPOutbox" + "\" + $newname) -Force
    if (!$error[0]) { add-content $LogFile "$datetime Copied $($file.fullname) to $($uncCPOutbox + "\" + $newname) + `r`n  "}
    if ($error[0]) { add-content $LogFile "$datetime --- [ERROR - $file.fullname] --- `r`n $Error`r`n "} 

}

if ($file.name -match "PND_997"){
if (!(test-path ("$uncCPOutbox" + $matches[1]))){new-item ("$uncCPOutbox" + $matches[1]) -type "directory"}
$newname = $file.name.split(".")[0] + "." + $file.name.split(".")[1]
$error.clear()
move-item $file.fullname ("$uncCPOutbox" + "\" + $newname) -Force
    if (!$error[0]) { add-content $LogFile "$datetime Copied $($file.fullname) to $($uncCPOutbox + "\" + $newname) + `r`n  "}
    if ($error[0]) { add-content $LogFile "$datetime --- [ERROR - $file.fullname] --- `r`n $Error`r`n "} 

}
}

##############################################################################################################################################
#                                                        MOVE MercuryGate FILES TO Transport Server                                          #
##############################################################################################################################################

foreach ($file in $MGOutfiles){ 

if ($file.name -match "MercuryGate_PLAN-SHIP*"){
if (!(test-path ("$uncMGOutbox" + $matches[1]))){new-item ("$uncMGOutbox" + $matches[1]) -type "directory"}
$newname = $file.name.split(".")[0] + "." + $file.name.split(".")[1]
$error.clear()
move-item $file.fullname ("$uncMGOutbox" + "\" + $newname) -Force
    if (!$error[0]) { add-content $LogFile "$datetime Copied $($file.fullname) to $($uncMGOutbox + "\" + $newname) + `r`n  "}
    if ($error[0]) { add-content $LogFile "$datetime --- [ERROR - $file.fullname] --- `r`n $Error`r`n "} 

}

if ($file.name -match "MercuryGate_SHIP*"){
if (!(test-path ("$uncMGOutbox" + $matches[1]))){new-item ("$uncMGOutbox" + $matches[1]) -type "directory"}
$Error.Clear()
move-item $file.fullname ("$uncMGOutbox" + "\" + $newname) -Force
    if (!$error[0]) { add-content $LogFile "$datetime Copied $($file.fullname) to $($uncMGOutbox + "\" + $newname) + `r`n  "}
    if ($error[0]) { add-content $LogFile "$datetime --- [ERROR - $file.fullname] --- `r`n $Error`r`n "} 

}

if ($file.name -match "MercuryGate_SWIFTV2-SHIP*"){
if (!(test-path ("$uncMGOutbox" + $matches[1]))){new-item ("$uncMGOutbox" + $matches[1]) -type "directory"}
$newname = $file.name.split(".")[0] + "." + $file.name.split(".")[1]
$error.clear()
move-item $file.fullname ("$uncMGOutbox" + "\" + $newname) -Force
    if (!$error[0]) { add-content $LogFile "$datetime Copied $($file.fullname) to $($uncMGOutbox + "\" + $newname) + `r`n  "}
    if ($error[0]) { add-content $LogFile "$datetime --- [ERROR - $file.fullname] --- `r`n $Error`r`n "} 

}

if ($file.name -match "MercuryGate_SWIFT-MBL*"){
if (!(test-path ("$uncMGOutbox" + $matches[1]))){new-item ("$uncMGOutbox" + $matches[1]) -type "directory"}
$newname = $file.name.split(".")[0] + "." + $file.name.split(".")[1]
$error.clear()
move-item $file.fullname ("$uncMGOutbox" + "\" + $newname) -Force
    if (!$error[0]) { add-content $LogFile "$datetime Copied $($file.fullname) to $($uncMGOutbox + "\" + $newname) + `r`n  "}
    if ($error[0]) { add-content $LogFile "$datetime --- [ERROR - $file.fullname] --- `r`n $Error`r`n "} 

}
}

##############################################################################################################################################
#                                                        MOVE McLeod FILES TO Transport Server                                          #
##############################################################################################################################################

foreach ($file in $MLOutfiles){ 

if ($file.name -match "MercuryGate_SWIFT-CAR-INV*"){
if (!(test-path ("$uncMLOutbox" + $matches[1]))){new-item ("$uncMLOutbox" + $matches[1]) -type "directory"}
$newname = $file.name.split(".")[0] + "." + $file.name.split(".")[1]
$error.clear
move-item $file.fullname ("$uncMLOutbox" + "\" + $newname) -Force
    if (!$error[0]) { add-content $LogFile "$datetime Copied $($file.fullname) to $($uncMLOutbox + "\" + $newname) + `r`n  "}
    if ($error[0]) { add-content $LogFile "$datetime --- [ERROR - $file.fullname] --- `r`n $Error`r`n "} 

}

if ($file.name -match "MercuryGate_SWIFT-CUST-INV*"){
if (!(test-path ("$uncMLOutbox" + $matches[1]))){new-item ("$uncMLOutbox" + $matches[1]) -type "directory"}
$newname = $file.name.split(".")[0] + "." + $file.name.split(".")[1]
$error.clear
move-item $file.fullname ("$uncMLOutbox" + "\" + $newname) -Force
    if (!$error[0]) { add-content $LogFile "$datetime Copied $($file.fullname) to $($uncMCOutbox + "\" + $newname) + `r`n  "}
    if ($error[0]) { add-content $LogFile "$datetime --- [ERROR - $file.fullname] --- `r`n $Error`r`n "} 

}
}

# Delete unc share.
net use $uncServer /delete
