# Define variables.

# Server UNC names.
$TransportServer = "\\stcphx-cleo3602"
$MiddlewareServer = "\\stcphx-altv3301"

# Inboxes on Transport Server, by Trading Partner.
$uncMGInbox = "$TransportServer\VLTrader\MercuryGate\inbox"
$uncSPSInbox = "$TransportServer\VLTrader\SPSCommerce\inbox"
$uncMLInbox = "$TransportServer\VLTrader\McLeod\inbox"

# Outboxes on Transport Server, by Trading Partner.
$uncIAOutbox = "$TransportServer\VLTrader\IntelligentAudit\outbox"
$uncMLOutbox = "$TransportServer\VLTrader\McLeod\outbox"

# Files on Middleware Server, by Trading Partner.
$MGfiles = gci $uncMGInbox\*.*
$SPSfiles = gci $uncSPSInbox\*.*
$MLfiles = gci $uncMLInbox\*.*

# Other variables.
$LogFile = ($MiddlewareServer + "\d$\Scripts\Logs\DMZGrab.log")
$username = "stcphx-cleo3602\Files"
$password = "P@swrd1"
$datetime = Get-Date 
 
# Set credentials and Inbox for unc share defined above in $TransportServer, $uncMGInbox, $username, and $password.
# I should probably move these variables out to a .cfg file, and consider encrypting the password. 
net use $TransportServer $password /USER:$username

# Each of these snipits copies files from the AS2/FTP server with specified name patterns to specified folders on the middleware server, and logs results.

##############################################################################################################################################
#                                                        MOVE MercuryGate FILES FROM Transport Server                                        #
##############################################################################################################################################

foreach ($file in $MGfiles){ 

if ($file.name -match "BIDATA*.*"){
if (!(test-path ("$MiddlewareServer\B2B\Inbound\MercuryGate\DeepExtract" + $matches[1]))){new-item ("$MiddlewareServer\B2B\Inbound\MercuryGate\DeepExtract" + $matches[1]) -type "directory"}
$newname = "MercuryGate_" + $file.name.split(".")[0] + "." + $file.name.split(".")[1]
$error.clear()
move-item $file.fullname ("$MiddlewareServer\B2B\Inbound\MercuryGate\DeepExtract" + $matches[1] + "\" + $newname) -Force
    if (!$error[0]) { add-content $LogFile "$datetime Copied $($file.fullname) to $('$MiddlewareServer\B2B\Inbound\MercuryGate\DeepExtract\' + $newname) + `r`n  "}
    if ($error[0]) { add-content $LogFile "$datetime --- [ERROR - $file.fullname] --- `r`n $Error`r`n "} 
}
 
if ($file.name -match "PLAN-SHIP*.*"){
if (!(test-path ("$MiddlewareServer\B2B\Inbound\MercuryGate\PlannedShipments" + $matches[1]))){new-item ("$MiddlewareServer\B2B\Inbound\MercuryGate\PlannedShipments" + $matches[1]) -type "directory"}
$newname = "MercuryGate_" + $file.name.split(".")[0] + "." + $file.name.split(".")[1]
$error.clear()
move-item $file.fullname ("$MiddlewareServer\B2B\Inbound\MercuryGate\PlannedShipments" + $matches[1] + "\" + $newname) -Force
    if (!$error[0]) { add-content $LogFile "$datetime Copied $($file.fullname) to $('$MiddlewareServer\B2B\Inbound\MercuryGate\PlannedShipments\' + $newname) + `r`n  "}
    if ($error[0]) { add-content $LogFile "$datetime --- [ERROR - $file.fullname] --- `r`n  $Error`r`n "} 
}

if ($file.name -match "SWIFT-CAR-INV*.*"){
if (!(test-path ("$MiddlewareServer\B2B\Inbound\MercuryGate\APInvoices" + $matches[1]))){new-item ("$MiddlewareServer\B2B\Inbound\MercuryGate\APInvoices" + $matches[1]) -type "directory"}
$newname = "MercuryGate_" + $file.name.split(".")[0] + "." + $file.name.split(".")[1]
$error.clear()
copy-item $file.fullname ("$MiddlewareServer\B2B\Outbound\McLeod\APInvoices\" + $newname) -Force
    if (!$error[0]) { add-content $LogFile "$datetime Copied $($file.fullname) to $('$MiddlewareServer\B2B\Outbound\McLeod\APInvoices\' + $newname) + `r`n  "}
    if ($error[0]) { add-content $LogFile "$datetime --- [ERROR - $file.fullname] --- `r`n  $Error`r`n "} 
$error.clear()
move-item $file.fullname ("$MiddlewareServer\B2B\Inbound\MercuryGate\APInvoices" + $matches[1] + "\" + $newname) -Force
    if (!$error[0]) { add-content $LogFile "$datetime Copied $($file.fullname) to $('$MiddlewareServer\B2B\Inbound\MercuryGate\APInvoices\' + $newname) + `r`n  "}
    if ($error[0]) { add-content $LogFile "$datetime --- [ERROR - $file.fullname] --- `r`n  $Error`r`n "} 

}
if ($file.name -match "SWIFT-CUST-INV*.*"){
if (!(test-path ("$MiddlewareServer\B2B\Inbound\MercuryGate\ARInvoices" + $matches[1]))){new-item ("$MiddlewareServer\B2B\Inbound\MercuryGate\ARInvoices" + $matches[1]) -type "directory"}
$newname = "MercuryGate_" + $file.name.split(".")[0] + "." + $file.name.split(".")[1]
$error.clear()
copy-item $file.fullname ("$MiddlewareServer\B2B\Outbound\McLeod\ARInvoices\" + $newname) -Force
    if (!$error[0]) { add-content $LogFile "$datetime Copied $($file.fullname) to $('$MiddlewareServer\B2B\Outbound\McLeod\ARInvoices\' + $newname) + `r`n  "}
    if ($error[0]) { add-content $LogFile "$datetime --- [ERROR - $file.fullname] --- `r`n  $Error`r`n "} 
$error.clear()
move-item $file.fullname ("$MiddlewareServer\B2B\Inbound\MercuryGate\ARInvoices" + $matches[1] + "\" + $newname) -Force
    if (!$error[0]) { add-content $LogFile "$datetime Copied $($file.fullname) to $('$MiddlewareServer\B2B\Inbound\MercuryGate\APInvoices\' + $newname) + `r`n  "}
    if ($error[0]) { add-content $LogFile "$datetime --- [ERROR - $file.fullname] --- `r`n  $Error`r`n "} 

}
if ($file.name -match "SWIFTENT*.*"){
if (!(test-path ("$MiddlewareServer\B2B\Inbound\MercuryGate\Enterprise" + $matches[1]))){new-item ("$MiddlewareServer\B2B\Inbound\MercuryGate\Enterprise" + $matches[1]) -type "directory"}
$newname = "MercuryGate_" + $file.name.split(".")[0] + "." + $file.name.split(".")[1]
$error.clear()
move-item $file.fullname ("$MiddlewareServer\B2B\Inbound\MercuryGate\Enterprise" + $matches[1] + "\" + $newname) -Force
    if (!$error[0]) { add-content $LogFile "$datetime Copied $($file.fullname) to $('$MiddlewareServer\B2B\Inbound\MercuryGate\Enterprise\' + $newname) + `r`n  "}
    if ($error[0]) { add-content $LogFile "$datetime --- [ERROR - $file.fullname] --- `r`n  $Error`r`n "} 

}
if ($file.name -match "SWIFTUSR*.*"){
if (!(test-path ("$MiddlewareServer\B2B\Inbound\MercuryGate\Users" + $matches[1]))){new-item ("$MiddlewareServer\B2B\Inbound\MercuryGate\Users" + $matches[1]) -type "directory"}
$newname = "MercuryGate_" + $file.name.split(".")[0] + "." + $file.name.split(".")[1]
$error.clear()
move-item $file.fullname ("$MiddlewareServer\B2B\Inbound\MercuryGate\Users" + $matches[1] + "\" + $newname) -Force
    if (!$error[0]) { add-content $LogFile "$datetime Copied $($file.fullname) to $('$MiddlewareServer\B2B\Inbound\MercuryGate\Users\' + $newname) + `r`n  "}
    if ($error[0]) { add-content $LogFile "$datetime --- [ERROR - $file.fullname] --- `r`n  $Error`r`n "} 

}
if ($file.name -match "SWIFTITEM*.*"){
if (!(test-path ("$MiddlewareServer\B2B\Inbound\MercuryGate\Items" + $matches[1]))){new-item ("$MiddlewareServer\B2B\Inbound\MercuryGate\Items" + $matches[1]) -type "directory"}
$newname = "MercuryGate_" + $file.name.split(".")[0] + "." + $file.name.split(".")[1]
$error.clear()
move-item $file.fullname ("$MiddlewareServer\B2B\Inbound\MercuryGate\Items" + $matches[1] + "\" + $newname) -Force
    if (!$error[0]) { add-content $LogFile "$datetime Copied $($file.fullname) to $('$MiddlewareServer\B2B\Inbound\MercuryGate\Items\' + $newname) + `r`n  "}
    if ($error[0]) { add-content $LogFile "$datetime --- [ERROR - $file.fullname] --- `r`n  $Error`r`n "} 

}
if ($file.name -match "SWIFT-MBL*.*"){
if (!(test-path ("$MiddlewareServer\B2B\Inbound\MercuryGate\Shipments" + $matches[1]))){new-item ("$MiddlewareServer\B2B\Inbound\MercuryGate\Shipments" + $matches[1]) -type "directory"}
$newname = "MercuryGate_" + $file.name.split(".")[0] + "." + $file.name.split(".")[1]
$error.clear()
move-item $file.fullname ("$MiddlewareServer\B2B\Inbound\MercuryGate\Shipments" + $matches[1] + "\" + $newname) -Force
    if (!$error[0]) { add-content $LogFile "$datetime Copied $($file.fullname) to $('$MiddlewareServer\B2B\Inbound\MercuryGate\Shipments\' + $newname) + `r`n  "}
    if ($error[0]) { add-content $LogFile "$datetime --- [ERROR - $file.fullname] --- `r`n  $Error`r`n "} 

}
if ($file.name -match "SWIFT-ORD*.*"){
if (!(test-path ("$MiddlewareServer\B2B\Inbound\MercuryGate\Orders" + $matches[1]))){new-item ("$MiddlewareServer\B2B\Inbound\MercuryGate\Orders" + $matches[1]) -type "directory"}
$newname = "MercuryGate_" + $file.name.split(".")[0] + "." + $file.name.split(".")[1]
$error.clear
move-item $file.fullname ("$MiddlewareServer\B2B\Inbound\MercuryGate\Orders" + $matches[1] + "\" + $newname) -Force
    if (!$error[0]) { add-content $LogFile "$datetime Copied $($file.fullname) to $('$MiddlewareServer\B2B\Inbound\MercuryGate\Orders\' + $newname) + `r`n  "}
    if ($error[0]) { add-content $LogFile "$datetime --- [ERROR - $file.fullname] --- `r`n  $Error`r`n "} 

}
if ($file.name -match "SWIFTSHIP*.*"){
if (!(test-path ("$MiddlewareServer\B2B\Inbound\MercuryGate\Shipments" + $matches[1]))){new-item ("$MiddlewareServer\B2B\Inbound\MercuryGate\Shipments" + $matches[1]) -type "directory"}
$newname = "MercuryGate_" + $file.name.split(".")[0] + "." + $file.name.split(".")[1]
$error.clear()
move-item $file.fullname ("$MiddlewareServer\B2B\Inbound\MercuryGate\Shipments" + $matches[1] + "\" + $newname) -Force
    if (!$error[0]) { add-content $LogFile "$datetime Copied $($file.fullname) to $('$MiddlewareServer\B2B\Inbound\MercuryGate\Shipments\' + $newname) + `r`n  "}
    if ($error[0]) { add-content $LogFile "$datetime --- [ERROR - $file.fullname] --- `r`n  $Error`r`n "} 

}
if ($file.name -match "SWIFTV2-SHIP*.*"){
if (!(test-path ("$MiddlewareServer\B2B\Inbound\MercuryGate\Shipments" + $matches[1]))){new-item ("$MiddlewareServer\B2B\Inbound\MercuryGate\Shipments" + $matches[1]) -type "directory"}
$newname = "MercuryGate_" + $file.name.split(".")[0] + "." + $file.name.split(".")[1]
$error.clear()
move-item $file.fullname ("$MiddlewareServer\B2B\Inbound\MercuryGate\Shipments" + $matches[1] + "\" + $newname) -Force
    if (!$error[0]) { add-content $LogFile "$datetime Copied $($file.fullname) to $('$MiddlewareServer\B2B\Inbound\MercuryGate\Shipments\' + $newname) + `r`n  "}
    if ($error[0]) { add-content $LogFile "$datetime --- [ERROR - $file.fullname] --- `r`n  $Error`r`n "} 

}

if ($file.name -match "STATUS*.*"){
if (!(test-path ("$MiddlewareServer\B2B\Inbound\MercuryGate\STATUS" + $matches[1]))){new-item ("$MiddlewareServer\B2B\Inbound\MercuryGate\STATUS" + $matches[1]) -type "directory"}
$newname = "MercuryGate_" + $file.name.split(".")[0] + "." + $file.name.split(".")[1]
$error.clear()
move-item $file.fullname ("$MiddlewareServer\B2B\Inbound\MercuryGate\STATUS" + $matches[1] + "\" + $newname) -Force
    if (!$error[0]) { add-content $LogFile "$datetime Copied $($file.fullname) to $('$MiddlewareServer\B2B\Inbound\MercuryGate\STATUS\' + $newname) + `r`n  "}
    if ($error[0]) { add-content $LogFile "$datetime --- [ERROR - $file.fullname] --- `r`n  $Error`r`n "} 
}

if ($file.name -match "IA_TRANS*.*"){
if (!(test-path ("$MiddlewareServer\B2B\Inbound\IA_TRANS" + $matches[1]))){new-item ("$MiddlewareServer\B2B\Inbound\IA_TRANS" + $matches[1]) -type "directory"}
$newname = "MercuryGate_" + $file.name.split(".")[0] + "." + $file.name.split(".")[1]
$error.status()
copy-item $file.fullname ($uncIAOutbox + "\" + $file.name) -Force
    if (!$error[0]) { add-content $LogFile "$datetime Copied $($file.fullname) to $($uncIAOutbox + $newname) + `r`n  "}
    if ($error[0]) { add-content $LogFile "$datetime --- [ERROR - $file.fullname] --- `r`n  $Error`r`n "} 
$error.clear()
move-item $file.fullname ("$MiddlewareServer\B2B\Inbound\IntelligentAudit\IA_TRANS" + $matches[1] + "\" + $newname) -Force
    if (!$error[0]) { add-content $LogFile "$datetime Copied $($file.fullname) to $('$MiddlewareServer\B2B\Inbound\IntelligentAudit\IA_TRANS\' + $newname) + `r`n  "}
    if ($error[0]) { add-content $LogFile "$datetime --- [ERROR - $file.fullname] --- `r`n  $Error`r`n "} 
}
}

##############################################################################################################################################
#                                                        MOVE SPSCommerce FILES FROM Transport Server                                        #
##############################################################################################################################################


foreach ($file in $SPSFiles){ 

if ($file.name -match "214*.*"){
if (!(test-path ("$MiddlewareServer\B2B\Inbound\SPSCommerce\214" + $matches[1]))){new-item ("$MiddlewareServer\B2B\Inbound\SPSCommerce\214" + $matches[1]) -type "directory"}
$newname = "SPS_" + $file.name.split(".")[0] + "." + $file.name.split(".")[1]
$error.clear()
move-item $file.fullname ("$MiddlewareServer\B2B\Inbound\SPSCommerce\214" + $matches[1] + "\" + $newname) -Force
    if (!$error[0]) { add-content $LogFile "$datetime Copied $($file.fullname) to $('$MiddlewareServer\B2B\Inbound\SPSCommerce\214' + $newname) + `r`n  "}
    if ($error[0]) { add-content $LogFile "$datetime --- [ERROR - $file.fullname] --- `r`n  $Error`r`n "} 
}

if ($file.name -match "940*.*"){
if (!(test-path ("$MiddlewareServer\B2B\Inbound\SPSCommerce\940" + $matches[1]))){new-item ("$MiddlewareServer\B2B\Inbound\SPSCommerce\940" + $matches[1]) -type "directory"}
$newname = "SPS_" + $file.name.split(".")[0] + "." + $file.name.split(".")[1]
$error.clear()
move-item $file.fullname ("$MiddlewareServer\B2B\Inbound\SPSCommerce\940" + $matches[1] + "\" + $newname) -Force
    if (!$error[0]) { add-content $LogFile "$datetime Copied $($file.fullname) to $('$MiddlewareServer\B2B\Inbound\SPSCommerce\940' + $newname) + `r`n  "}
    if ($error[0]) { add-content $LogFile "$datetime --- [ERROR - $file.fullname] --- `r`n  $Error`r`n "} 
}

if ($file.name -match "945*.*"){
if (!(test-path ("$MiddlewareServer\B2B\Inbound\SPSCommerce\945" + $matches[1]))){new-item ("$MiddlewareServer\B2B\Inbound\SPSCommerce\945" + $matches[1]) -type "directory"}
$newname = "SPS_" + $file.name.split(".")[0] + "." + $file.name.split(".")[1]
$error.clear()
move-item $file.fullname ("$MiddlewareServer\B2B\Inbound\SPSCommerce\945" + $matches[1] + "\" + $newname) -Force
    if (!$error[0]) { add-content $LogFile "$datetime Copied $($file.fullname) to $('$MiddlewareServer\B2B\Inbound\SPSCommerce\9445' + $newname) + `r`n  "}
    if ($error[0]) { add-content $LogFile "$datetime --- [ERROR - $file.fullname] --- `r`n  $Error`r`n "} 

if ($file.name -match "997*.*"){
if (!(test-path ("$MiddlewareServer\B2B\Inbound\SPSCommerce\997" + $matches[1]))){new-item ("$MiddlewareServer\B2B\Inbound\SPSCommerce\997" + $matches[1]) -type "directory"}
$newname = "SPS_" + $file.name.split(".")[0] + "." + $file.name.split(".")[1]
$error.clear()
move-item $file.fullname ("$MiddlewareServer\B2B\Inbound\SPSCommerce\997" + $matches[1] + "\" + $newname) -Force
    if (!$error[0]) { add-content $LogFile "$datetime Copied $($file.fullname) to $('$MiddlewareServer\B2B\Inbound\SPSCommerce\997' + $newname) + `r`n  "}
    if ($error[0]) { add-content $LogFile "$datetime --- [ERROR - $file.fullname] --- `r`n  $Error`r`n "} 
}

}

##############################################################################################################################################
#                                                        MOVE McLeod FILES FROM Transport Server                                             #
##############################################################################################################################################

foreach ($file in $MLFiles){ 

if ($file.name -match "201*.out"){
if (!(test-path ("$MiddlewareServer\B2B\Inbound\McLeod\Response" + $matches[1]))){new-item ("$MiddlewareServer\B2B\Inbound\McLeod\Response" + $matches[1]) -type "directory"}
$newname = "ML_" + $file.name.split(".")[0] + "." + $file.name.split(".")[1]
$error.clear()
move-item $file.fullname ("$MiddlewareServer\B2B\Inbound\McLeod\Response" + $matches[1] + "\" + $newname) -Force
    if (!$error[0]) { add-content $LogFile "$datetime Copied $($file.fullname) to $('$MiddlewareServer\B2B\Inbound\McLeod\Respons' + $newname) + `r`n  "}
    if ($error[0]) { add-content $LogFile "$datetime --- [ERROR - $file.fullname] --- `r`n  $Error`r`n "} 
}
}

# Delete unc share.
net use $TransportServer /delete}
