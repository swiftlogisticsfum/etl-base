﻿# Set the value $oldTime.
# $oldTime represents the minimum age of files to delete.
$oldTime = [int]30 # 30 days.

# Clear out the system value $error.
$error.clear()

# Delete files older than $oldTime days old for every directorie listed in d:\scripts\config\pathList.cfg.
foreach ($path in Get-Content "\\swift.com\shared_data\LogisticsB2B\repository\Altova_ETL\Powershell_Scripts\Config\ArchivePathList.cfg") {
    $date = get-date

	# Log information of what it is about to do.
	add-content \\swift.com\shared_data\LogisticsB2B\repository\Altova_ETL\Powershell_Scripts\Logs\PurgeFiles.log "$date Attempting to delete files older than $oldTime days, in the folder $path" 
	
    # Delete the old files
	Get-ChildItem $path -Recurse -Include "*.*" | WHERE {($_.CreationTime -le $(Get-Date).AddDays(-$oldTime))} | Remove-Item -Force
    
    # Log results.
    if (!$error[0]) {add-content \\swift.com\shared_data\LogisticsB2B\repository\Altova_ETL\Powershell_Scripts\Logs\PurgeFiles.log "$date Files deleted successfully!`r`n"}
    if ($error[0]) {add-content \\swift.com\shared_data\LogisticsB2B\repository\Altova_ETL\Powershell_Scripts\Logs\PurgeFiles.log "$date [ERROR] $Error`r`n"}
    
    # Clear out the system value $error.
    $error.clear()
}