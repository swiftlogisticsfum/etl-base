﻿# Set the value $oldTime.
# $oldTime represents the minimum age of files to delete.
$oldTime = [int]30 # 30 days.
$Archivepath = '\\swift.com\shared_data\LogisticsB2B\ArchiveBackup\'

# Clear out the system value $error.
$error.clear()

# Move files older than $oldTime days old for every directory listed in d:\scripts\config\pathList.cfg.
foreach ($path in Get-Content "\\swift.com\shared_data\LogisticsB2B\Production\Repository\Powershell_Scripts\Config\ArchivePathList.cfg") {
    $date = get-date
        foreach ($File in Get-ChildItem $path -Recurse -Include "*.*","*" | WHERE {($_.CreationTime -le $(Get-Date).AddDays(-$oldTime))}) {

	        # Log information of what it is about to do
	        add-content \\swift.com\shared_data\LogisticsB2B\Production\Repository\Powershell_Scripts\Logs\ArchiveFiles.log "$date Attempting to archive files older than $oldTime days, in the folder $path" 
	
            # Move the old files
	        Move-Item $File $Archivepath
    
            # Log results.
            if (!$error[0]) {add-content \\swift.com\shared_data\LogisticsB2B\repository\Altova_ETL\Powershell_Scripts\Logs\ArchiveFiles.log "$date Files moved successfully!`r`n"}
            if ($error[0]) {add-content \\swift.com\shared_data\LogisticsB2B\repository\Altova_ETL\Powershell_Scripts\Logs\ArchiveFiles.log "$date [ERROR] $Error`r`n"}
    
            # Clear out the system value $error.
            $error.clear()
    }
}