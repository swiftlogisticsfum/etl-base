﻿#set variables
$204InPath = "\\swift.com\shared_data\LogisticsB2B\Production\Altova\Inbound\CarrierPoint\204\"
$214InPath = "\\swift.com\shared_data\LogisticsB2B\Production\Altova\Inbound\CarrierPoint\214\"
$CarrierPointInPath = "\\swift.com\shared_data\LogisticsB2B\Production\Altova\Inbound\CarrierPoint\"
$EDIFile = gci $CarrierPointInPath
$LogPath = "\\stcphx-altv3301\d$\scripts\Logs"
$Log = "Parse_EDI_ST.log"
$error.clear()
$Global:204
$Global:214
$MailRecipient = "Ponderay Ops/Phoenix/Swift <Ponderay_Ops@swifttrans.com>; Chad <chad_moscarella@swifttrans.com>"
#$MailRecipient = "Chad <chad_moscarella@swifttrans.com>"
$MailSender = "PROD FlowForce ETL <PROD_FlowForceETL@SwiftLogistics.com"
$MailBody = "A load tender indicating a canceled shipment has been received via EDI and is being processed by into the Integration Layer.`r`nThe EDI file that triggered this alert is attached."
$MailServer = "mailrelay.swifttrans.com"

#perform the following on each file (not folder) in $CarrierPointInPath
foreach ($file in (gci $CarrierPointInPath | where {!$_.PsIsContainer}))
{    

#housekeeping
$MailAttachment = $file
clear-item variable:204
clear-item variable:214
clear-item variable:997

#check document type
$204 = Select-String ST*204* $file -SimpleMatch
$214 = Select-String ST*214* $file -SimpleMatch
$997 = Select-String ST*997* $file -SimpleMatch
$StatusCode = Select-String B2A*01*LT $file -SimpleMatch
$Carrier = Select-String "B2\*\*" $file
$CarrierSCAC = $Carrier.ToString().split('*')
$Shipment = $CarrierSCAC[4]
$MailSubject = "Ponderay shipment #SL$Shipment CANCELLED."

cd $CarrierPointInPath
    
#copy 997, 204 and 214 files to appropriate directories with new name, then reset $Stamp to prevent duplicate file names.


if ($204) 
    { 
        
            if ($CarrierSCAC[2] -ne "SWFG") 
    
                {

                if ($StatusCode.pattern.substring(4)='01') 
                    {
                        Send-mailmessage -To $MailRecipient -From $MailSender -subject $MailSubject -body $MailBody -Attachments $MailAttachment -SmtpServer $MailServer
                    }

                }

        $Stamp = get-date -format MMddyyyy_hhmmss_ffffff
        $newname = "CP_204_$shipment_$stamp.204"
        Move-Item -path $File -Destination \\swift.com\shared_data\LogisticsB2B\Production\Altova\Inbound\CarrierPoint\PND_204\$newname | Out-Null
        if ($error)
            { Write-Output "$Stamp [ERROR] - $error `r" | Out-File -filepath "$LogPath\$Log"  -NoClobber -append}
        else
            { Write-Output "$Stamp [INFO] $File moved and renamed to $CarrierPointInPath$newname `r" | Out-File -filepath "$LogPath\$Log" -NoClobber -append }
        $Error.Clear()
        $204.Clear()
        
    }

if ($214) 
    { 
        $Stamp = get-date -format MMddyyyy_hhmmss_ffffff
        $newname = "CP_214_$shipment_$stamp.214"
        Move-Item -path $File -Destination \\swift.com\shared_data\LogisticsB2B\Production\Altova\Inbound\CarrierPoint\PND_214\$newname | Out-Null
        
        if ($error) 
            { Write-Output "$Stamp [ERROR] - $error `r" | Out-File -filepath "$LogPath\$Log"  -NoClobber -append}
        else
            { Write-Output "$Stamp [INFO] $File moved and renamed to $CarrierPointInPath$newname `r" | Out-File -filepath "$LogPath\$Log" -NoClobber -append}
        $Error.Clear()
        $214.Clear()

    }
if ($997) 
    { 
        $Stamp = get-date -format MMddyyyy_hhmmss_ffffff
        $newname = "CP_997_$shipment_$stamp.997"
        Move-Item -path $File -Destination \\swift.com\shared_data\LogisticsB2B\Production\Altova\Inbound\CarrierPoint\PND_997\$newname | Out-Null
        
        if ($error) 
            { Write-Output "$Stamp [ERROR] - $error `r" | Out-File -filepath "$LogPath\$Log"  -NoClobber -append}
        else
            { Write-Output "$Stamp [INFO] $File moved and renamed to $CarrierPointInPath$newname `r" | Out-File -filepath "$LogPath\$Log" -NoClobber -append}
        $Error.Clear()
        $997.Clear()
    }
}
