﻿#FTP Server Information - SET VARIABLES
    $ftphost = "ftp://atlcom.mercurygate.net" 
    $user = 'swiftadmin' 
    $pass = 'kQn4!yJ9'
    $ftpfolders = get-content -path \\swift.com\shared_data\LogisticsB2B\Production\Repository\Powershell_Scripts\Config\FTPFolders.cfg 
    $target = "\\swift.com\shared_data\logisticsb2b\Production\MercuryGateFTP\"
        
#SET CREDENTIALS
    $credentials = new-object System.Net.NetworkCredential($user, $pass)

function Get-FtpDir ($url,$credentials) {
        $request = [Net.WebRequest]::Create($url)
        $request.Method = [System.Net.WebRequestMethods+FTP]::ListDirectory
        if ($credentials) { $request.Credentials = $credentials }
        $response = $request.GetResponse()
        $reader = New-Object IO.StreamReader $response.GetResponseStream() 
        $reader.ReadToEnd()
        $reader.Close()
        $response.Close()
    }

#SET FOLDER PATH
foreach ($folder in $ftpfolders) { 
    $folder | %{$token = $_.split("`/")}
    $ftppath= $ftphost + "/" + $folder + "/"
    $targetfolder = $target + $ftpfolder + "\"
    $destinationfolder = ($target + $token[0] + "\" + $token[1])
    $Allfiles=Get-FTPDir -url $ftppath -credentials $credentials
    $files = ($Allfiles -split "`r`n")
    $files 
    $webclient = New-Object System.Net.WebClient 
    $webclient.Credentials = New-Object System.Net.NetworkCredential($user,$pass) 
    $counter = 0
    foreach ($file in ($files | where {$_ -like "*.*"})){
        $source=$ftppath + $file  
        $destination = ($destinationfolder + "\" + $file)
        $fileexists = Test-Path $destination
        if (-not $fileexists) { $webclient.DownloadFile($source, $destination) }

#PRINT FILE NAME AND COUNTER
        $counter++
        $counter
        $source
    }

}