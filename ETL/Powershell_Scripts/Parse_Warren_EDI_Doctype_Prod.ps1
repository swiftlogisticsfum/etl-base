﻿#set variables
$940InPath = "\\swift.com\shared_data\LogisticsB2B\Production\Altova\Inbound\SPSCommerce\940\"
$945InPath = "\\swift.com\shared_data\LogisticsB2B\Production\Altova\Inbound\SPSCommerce\945\"
$SPSCommerceInPath = "\\swift.com\shared_data\LogisticsB2B\Production\Altova\Inbound\SPSCommerce\"
$EDIFile = gci $SPSCommerceInPath
$LogPath = "\\stcphx-altv3301\d$\scripts\Logs"
$Log = "Parse_EDI_ST.log"
$error.clear()
$Global:940
$Global:945

#perform the following on each file (not folder) in $SPSCommerceInPath
foreach ($file in (gci $SPSCommerceInPath | where {!$_.PsIsContainer}))
{    

#check document type
$940 = Select-String ST*940* $file -SimpleMatch
$945 = Select-String ST*945* $file -SimpleMatch

cd $SPSCommerceInPath
    
#copy 940 and 945 files to appropriate directories with new name, then reset $Stamp to prevent duplicate file names.
if ($940) 
    { 
        $Stamp = get-date -format MMddyyyy_hhmmss_ffffff
        $newname = "SPS_940_$stamp.x12"
        Move-Item -path $File -Destination \\swift.com\shared_data\LogisticsB2B\Production\Altova\Inbound\SPSCommerce\940\$newname
        if ($error)
            { Write-Output "$Stamp [ERROR] - $error `r" | Out-File -filepath "$LogPath\$Log"  -NoClobber -append}
        else
            { Write-Output "$Stamp [INFO] $File moved and renamed to $SPSCommerceInPath$newname `r" | Out-File -filepath "$LogPath\$Log" -NoClobber -append }
        $Error.Clear()
        clear-item variable:940
        
    }

if ($945) 
    { 
        $Stamp = get-date -format MMddyyyy_hhmmss_ffffff
        $newname = "SPS_945_$stamp.x12"
        Move-Item -path $File -Destination \\swift.com\shared_data\LogisticsB2B\Production\Altova\Inbound\SPSCommerce\945\$newname
        
        if ($error) 
            { Write-Output "$Stamp [ERROR] - $error `r" | Out-File -filepath "$LogPath\$Log"  -NoClobber -append}
        else
            { Write-Output "$Stamp [INFO] $File moved and renamed to $SPSCommerceInPath$newname `r" | Out-File -filepath "$LogPath\$Log" -NoClobber -append}
        $Error.Clear()
        clear-item variable:945
    }
}