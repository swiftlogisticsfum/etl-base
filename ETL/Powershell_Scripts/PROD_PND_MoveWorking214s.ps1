﻿$214WorkingPath = '\\swift.com\shared_data\LogisticsB2B\Production\Altova\Outbound\CarrierPoint\PND_214\WORKING\'
$214CompletePath = '\\swift.com\shared_data\LogisticsB2B\Production\VLTrader\TradingPartners\CarrierPoint\Outbox\'
$Complete214s = Get-ChildItem $214WorkingPath -Recurse | Where-Object {$_.Length -gt 0KB}

cd $214WorkingPath
Move-Item $Complete214s $214CompletePath
