﻿#set variables
$940InPath = "\\swift.com\shared_data\LogisticsB2B\Production\Altova\Inbound\SPSCommerce\940\"
$945InPath = "\\swift.com\shared_data\LogisticsB2B\Production\Altova\Inbound\SPSCommerce\945\"
$SPSCommerceInPath = "\\swift.com\shared_data\LogisticsB2B\Production\Altova\Inbound\SPSCommerce\"
$EDIFile = gci $SPSCommerceInPath
$LogPath = "\\swift.com\shared_data\LogisticsB2B\Repository\Altova_ETL\Powershell_Scripts\Logs"
$Log = "Parse_EDI_ST.log"
$error.clear()


#perform the following on each file (not folder) in $SPSCommerceInPath
foreach ($file in (gci $SPSCommerceInPath | where {!$_.PsIsContainer})) 
{    

$DateTime = get-date -Format yyyyMMdd
$940 = Select-String ST*940* $file -SimpleMatch
$945 = Select-String ST*945* $file -SimpleMatch
$OrderStatusCode = Select-String "W05\*" $file
$OrderStatusCodeTokens = $OrderStatusCode.ToString().split('*')
$OrderType = Select-String "N9\*OT\*" $file
$DeliveryDate = Select-String "G62\*02" $file
$ShipDate = Select-String "G62\*10" $file

if ($DeliveryDate) { $DeliveryDateTokens = $DeliveryDate.ToString().split('*') }

$OrderTypeTokens = $OrderType.ToString().Split('*')
$Shipment = $OrderStatusCodeTokens[2]
$MailSubject1 = "Warren Distribution shipment $Shipment CANCELED."
$MailSubject2 = "Warren Distribution shipment $Shipment is scheduled to ship soon."
$MailSubject3 = "Warren Distribution shipment $Shipment has a delivery date in the past."
$MailSubject4 = "Warren Distribution shipment $Shipment CHANGED."
$MailRecipient = "Chad <chad_moscarella@swifttrans.com>", "Sree <lakshmi_akepati@swifttrans.com>", "Warren Operations <warrenops@swifttrans.com>"
$MailSender = "PROD Middleware Scripts <PROD_PowerShell@SwiftLogistics.com>"
$MailBody1 = "Order $Shipment has been CANCELED`r`nThe EDI file that triggered this alert is attached. `r`nHave a wonderful day!"
$MailBody2 = "Order $Shipment is either a same day order, a two day order or a three day order.`r`nThe EDI file that triggered this alert is attached. `r`nHave a wonderful day!"
$MailBody3 = "Order $Shipment has a delivery date in the past."
$MailBody4 = "Order $Shipment has changed."
$MailServer = "mailrelay.swifttrans.com"
$MailAttachment = $file

cd $SPSCommerceInPath
    
#copy 940 and 945 files to appropriate directories with new name, then reset $Stamp to prevent duplicate file names.
if ($940) 
    
        {

        if ($OrderStatusCodeTokens[1] -eq 'F'){
                Send-mailmessage -To $MailRecipient -From $MailSender -subject $MailSubject1 -body $MailBody1 -Attachments $MailAttachment -SmtpServer $MailServer
            }<#
            
        if ($OrderTypeTokens[2] -eq '2DC' -or $OrderTypeTokens[2] -eq '3DC' -or $OrderTypeTokens[2] -eq '2DN' -or $OrderTypeTokens[2] -eq '3DN' -or $OrderTypeTokens[2] -eq 'SDN'){
                Send-mailmessage -To $MailRecipient -From $MailSender -subject $MailSubject2 -body $MailBody2 -Attachments $MailAttachment -SmtpServer $MailServer
            }

        if ($DeliveryDateTokens -gt '0') {
         
            if ($DeliveryDateTokens[2] -lt $DateTime) {
                    Send-mailmessage -To $MailRecipient -From $MailSender -subject $MailSubject3 -body $MailBody3 -Attachments $MailAttachment -SmtpServer $MailServer
                }

                $DeliveryDateTokens.Clear()

        }

        if ($OrderStatusCodeTokens[1] -eq 'R'){
                Send-mailmessage -To $MailRecipient -From $MailSender -subject $MailSubject4 -body $MailBody4 -Attachments $MailAttachment -SmtpServer $MailServer
            } #>

        $Stamp = get-date -format MMddyyyy_hhmmss_ffffff
        $newname = "SPS_940_$Shipment-$stamp.940"
        Move-Item -path $File -Destination \\swift.com\shared_data\LogisticsB2B\Production\Altova\Inbound\SPSCommerce\940\$newname
        if ($error)
            { Write-Output "$Stamp [ERROR] - $error `r" | Out-File -filepath "$LogPath\$Log"  -NoClobber -append}
        else
            { Write-Output "$Stamp [INFO] $File moved and renamed to $SPSCommerceInPath$newname `r" | Out-File -filepath "$LogPath\$Log" -NoClobber -append }
        $Error.Clear()
           
        }

if ($945) 

    { 
        $Stamp = get-date -format MMddyyyy_hhmmss_ffffff
        $newname = "SPS_945_$Shipment-$stamp.x12"
        Move-Item -path $File -Destination \\swift.com\shared_data\LogisticsB2B\Production\Altova\Inbound\SPSCommerce\945\$newname
        
        if ($error) 
            { Write-Output "$Stamp [ERROR] - $error `r" | Out-File -filepath "$LogPath\$Log"  -NoClobber -append}
        else
            { Write-Output "$Stamp [INFO] $File moved and renamed to $SPSCommerceInPath$newname `r" | Out-File -filepath "$LogPath\$Log" -NoClobber -append}
        $Error.Clear()
        
    }

#housekeeping
clear-item variable:940
clear-item variable:945
}