﻿$FilePath = "\\swift.com\shared_data\LogisticsB2B\Production\VLTrader\Archive\Received\SPSCommerce"
$LogPath = "\\swift.com\shared_data\LogisticsB2B\Production\Repository\Powershell_Scripts\Logs"
$Log = "OrderList.csv"
$Date = get-date -Format yyyyMMdd
$DateModifier = read-host "Please enter date modifier (1-31)"


foreach ($File in (gci $FilePath | where {!$_.PsIsContainer})) 

    {  

    $W05 = Select-String "W05\*" $File
    $W76 = Select-String "W76\*" $File
    If ($W76)

        { $W76Tokens = $W76.ToString().split('*') }

    If ($W05)

        { $W05Tokens = $W05.ToString().split('*') }

    $Shipment = $W05Tokens[2]
    $GS = Select-String "GS\*" $File
    $GSTokens = $GS.ToString().split('*')
    $FileDate = $GSTokens[4]
    $940Type = $W05Tokens[1]
    $Shipment = $W05Tokens[2]
    $Weight = $W76Tokens[2]

    $940 = Select-String ST*940* $File -SimpleMatch 

    if ($940 -and $940Type -eq "N" -and ($Weight -ge 150) -and ($FileDate -ge $Date - $DateModifier))
        
        { Write-Output "$Shipment " | Out-File -Filepath "$LogPath\$Log"  -NoClobber -append }

    }
        