﻿$McLeodResponsePath = "\\swift.com\shared_data\LogisticsB2B\Production\VLTrader\TradingPartners\McLeod\Inbox\"
$SearchString = "<Status>FAILED</Status>"
$logpath = "\\stcphx-altv3301\d$\scripts\logs"
$log = "McLeod_Response_Parser.log"
        
#perform the following on each file (not folder) in $McLeodResponsePath
foreach ($file in (gci $McLeodResponsePath | where {!$_.PsIsContainer}))

{    

#housekeeping

$Error.Clear()
	
#check documented job status
$Failed = Select-String "<Status>FAILED</Status>" $file -SimpleMatch

if ($Failed) 
   
    {
        $Stamp = get-date -format MMddyyyy_hhmmss_ffffff
        $newname = "McLeod_Response_$stamp.xml"
        Move-Item -path $File.FullName -Destination \\swift.com\shared_data\LogisticsB2B\Production\altova\Archive\Error\McLeod\$newname 
        if ($error)
            { Write-Output "$Stamp [ERROR] - $error `r" | Out-File -filepath "$LogPath\$Log" -append}
        else
            { Write-Output "$Stamp [INFO] $File moved and renamed $file to \\swift.com\shared_data\LogisticsB2B\Production\altova\Archive\Error\McLeod\InDB\$newname `r" | Out-File -filepath "$LogPath\$Log" -append }
        $Error.Clear()

    }


if (-not $Failed) 
   
    {

        $Attachment = ($file)
        $Stamp = get-date -format MMddyyyy_hhmmss_ffffff
        $newname = "McLeod_Response_$stamp.xml"
        Move-Item -path $File.FullName -Destination \\swift.com\shared_data\LogisticsB2B\Production\altova\Archive\Processed\McLeod\$newname 
        if ($error)
            { Write-Output "$Stamp [ERROR] - $error `r" | Out-File -filepath "$LogPath\$Log" -append}
        else
            { Write-Output "$Stamp [INFO] $File moved and renamed $file to \\swift.com\shared_data\LogisticsB2B\Production\altova\Archive\Processed\McLeod\InDB\$newname `r" | Out-File -filepath "$LogPath\$Log" -append }
        $Error.Clear()


    }
clear-item variable:Failed
}
   


  