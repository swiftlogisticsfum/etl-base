﻿$FlowForceLogFile = '\\stcphx-altv3601\c$\ProgramData\Altova\FlowForceServer2016\data\flowforce.log'
$DateTime = Get-Date
$IsLogOld = Test-Path $FlowForceLogFile -OlderThan $DateTime.AddDays(0).AddHours(-1).AddMinutes(0)
$MailRecipient = "Chad <chad_moscarella@swifttrans.com>","Sree <lakshmi_akepati@swifttrans.com>"
$MailSender = "PROD FlowForce ETL <PROD_FlowForceETL@SwiftLogistics.com>"
$MailSubject = "The FlowForce log has not been updated in over an hour!"
$MailBody = "As of $DateTime, the FlowForce server log (\\stcphx-altv3601\c$\ProgramData\Altova\FlowForceServer2016\data\flowforce.log) is getting stale, this is indicative of a service failure. Check on the server stcphx-altv3601 to be sure all Altova services are running.`r`n`r`nFlowForce jobs are configured to be executed as frequently as every minute. Every time a Flowforce job executes, a log entry is created. If the log hasn't been updated in an hour, it is likely that jobs have not executed for an hour."
$MailServer = "mailrelay.swifttrans.com"

if ($IsLogOld -eq 'True')
    {
        Send-mailmessage -To $MailRecipient -From $MailSender -subject $MailSubject -body $MailBody -SmtpServer $MailServer
    }