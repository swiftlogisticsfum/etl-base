# README #

### What is this repository for? ###

This repository is to house ETL mappings related to the Altova Middleware in use at Swift Logistics LLC. The mapping files are in MapForce format, and many are accompanied by EDI or XML files.

Version 1 uses the tables in mcleoddb\Altova_Prod and will be depreciated upon deployment of V.2. 

Version 2 is in development using tables in mcleoddb\Altova_Prod_V2

### How do I get set up? ###

Install SourceTree from this site.

Clone the Repository "Altova Middleware v2" locally.

Install Altova Mission Kit.

### Contribution guidelines ###

Use task based branching - all tasks should be entered into JIRA Software.

Don't forget to commit and merge changes frequently. 

### Who do I talk to? ###

Chad Moscarella - 480-287-0560